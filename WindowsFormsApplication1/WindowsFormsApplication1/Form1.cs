﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public int Npoints = 100;
        public float o_x_pix = 500;
        public float o_y_pix = 350;
        public float M_x = 450;
        public float M_y = 300;

        public Form1()
        {
            InitializeComponent();
        }
        public float a, b, x_min, x_max;
        float Function_of_graph(float x)
        {
            float Function;
            Function = a * x + b;
            return Function;
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            float step_x = (x_max - x_min) / Npoints;
            float x_max_abs = Math.Abs(x_max);
            if (x_max_abs < Math.Abs(x_min)) x_max_abs = Math.Abs(x_min);
            float x_0, y_0, x_1, y_1, x_0_pix, y_0_pix, x_1_pix, y_1_pix;
            float y_min, y_max;
            x_0 = x_min; y_0 = Function_of_graph(x_0);
            y_min = y_0; y_max = y_0; int i;
            for (i = 1; i <= (Npoints - 1); i++)
            {
                x_1 = x_min + i * step_x;
                y_1 = Function_of_graph(x_1);
                if (y_min > y_1) y_min = y_1;
                if (y_max < y_1) y_max = y_1;
            }
            x_1 = x_max; y_1 = Function_of_graph(x_1);
            if (y_min > y_1) y_min = y_1;
            if (y_max < y_1) y_max = y_1;
            float y_max_abs = Math.Abs(y_max);
            if (y_max_abs < Math.Abs(y_min)) y_max_abs = Math.Abs(y_min);
            float x_point_end, x_point_end_pix; x_point_end = 1;
            x_point_end_pix = x_point_end * M_x + o_x_pix;
            Pen greenPen_x = new Pen(Color.Green, 2);
            PointF point1 = new PointF(-1 * M_x + o_x_pix, o_y_pix);
            PointF point2 = new PointF(x_point_end_pix, o_y_pix);
            e.Graphics.DrawLine(greenPen_x, point1, point2);
            float span_y = y_max - y_min;
            int N_step_grid_y = 20;
            float step_grid_y, step_grid_y_pix;
            step_grid_y = (float)2 / N_step_grid_y;
            step_grid_y_pix = step_grid_y * M_y;
            Pen redPen = new Pen(Color.Red, 1);
            int j_y; float y1, y1_pix;
            for (j_y = 1; j_y <= (N_step_grid_y / 2); j_y++)
            {
                y1 = j_y * step_grid_y;
                y1_pix = o_y_pix + j_y * step_grid_y_pix;
                PointF point3 = new PointF(-1 * M_x + o_x_pix, y1_pix);
                PointF point4 = new PointF(x_point_end_pix, y1_pix);
                e.Graphics.DrawLine(redPen, point3, point4);
            }
            for (j_y = 1; j_y <= (N_step_grid_y / 2); j_y++)
            {
                y1_pix = o_y_pix - j_y * step_grid_y * M_y;
                PointF point5 = new PointF(-1 * M_x + o_x_pix, y1_pix);
                PointF point6 = new PointF(x_point_end_pix, y1_pix);
                e.Graphics.DrawLine(redPen, point5, point6);
            }
            float y_point_end, y_point_end_pix; y_point_end = 1;
            y_point_end_pix = y_point_end * M_y + o_y_pix;
            Pen greenPen_y = new Pen(Color.Green, 2);
            PointF point7 = new PointF(o_x_pix, -1 * M_y + o_y_pix);
            PointF point8 = new PointF(o_y_pix, y_point_end_pix);
            e.Graphics.DrawLine(greenPen_y, point7, point8);
            float span_x = x_max - x_min;
            int N_step_grid_x = 20;
            float step_grid_x = 0.1F, step_grid_x_pix;
            step_grid_x_pix = step_grid_x * M_x;
            Pen redPen_y = new Pen(Color.Red, 1);
            int j_x; float x1, x1_pix;
            for (j_x = 1; j_x <= (N_step_grid_x / 2); j_x++) ;
            {
                x1 = j_x * step_grid_x;
                x1_pix = o_x_pix + j_x * step_grid_x_pix;
                PointF point9 = new PointF(x1_pix, -1 * M_y + o_y_pix);
                PointF point10 = new PointF(x1_pix, y_point_end_pix);
                e.Graphics.DrawLine(greenPen_y, point9, point10);
            }
            for (j_x = 1; j_x <= (N_step_grid_x / 2); j_x++) ;
            {
                x1 = j_x * step_grid_x;
                x1_pix = o_x_pix + j_x * step_grid_x_pix;
                PointF point11 = new PointF(x1_pix, -1 * M_y + o_y_pix);
                PointF point12 = new PointF(x1_pix, y_point_end_pix);
                e.Graphics.DrawLine(greenPen_y, point11, point12);
            }
            int n; float p1 = 1, p2; string msg;
            for (n = 0; n <= 9; n++)
            {
                p2 = p1 - n * 0.1F;
                msg = "" + p2.ToString() + "";
                e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix - 35, o_y_pix - 323 + n * step_grid_y_pix);
            }
            p1 = 0;
            for (n = 1; n <= 10; n++)
            {
                p2 = p1 - n * 0.1F;
                msg = "" + p2.ToString() + "";
                e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix - 40, o_y_pix - 23 + n * step_grid_y_pix);
            }
            p1 = 0;
            for (n = 0; n <= 10; n++)
            {
                p2 = p1 + n * 0.1F;
                msg = "" + p2.ToString() + "";
                e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix - 0+n * step_grid_x_pix, o_y_pix-0);
            }
            p1 = 0;
            for (n = 1; n <= 10; n++)
            {
                p2 = p1 - n * 0.1F;
                msg = "" + p2.ToString() + "";
                e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix - 39 - n * step_grid_x_pix, o_y_pix - 0);
            }
            msg = "y'=y/|y_max|;";
            e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix - 5, o_y_pix - 333);
            msg = "|y_max|=" + y_max_abs.ToString() + ";";
            e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix + 170, o_y_pix - 333);
            msg = "y_max=" + y_max_abs.ToString() + ";";
            e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix + 380, o_y_pix - 333);
            msg = "y_max=" + y_max_abs.ToString() + ";";
            e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix + 455, o_y_pix - 300);
            msg = "x'=x/|x_max|";
            e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix + 455, o_y_pix - 30);
            msg = "y_max=" + x_max_abs.ToString() + "";
            e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix + 455, o_y_pix +40);
            msg = "y_max=" + y_max_abs.ToString() + ";";
            e.Graphics.DrawString(msg, this.Font, Brushes.Blue, o_x_pix + 455, o_y_pix - 140);
            x_0 = x_min; x_0_pix = x_0 / x_max_abs * M_x + o_x_pix;
            y_0 = -(Function_of_graph(x_0));
            y_0_pix = y_0 / y_max_abs * M_y + o_y_pix;
            Pen blackPen= new Pen (Co
        }
    }
}
