﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Product
{
    public class Product
    {
        private int productID;
        private string productName;
        public Product(int productID, string productName)
        {
            this.productID = productID;
            this.productName = productName;
        }

        public int ProductID
        {
            get
            {
                return productID;
            }
           
        }
        public string ProductName
        {
            get
            {
                return productName;
            }
            
        }

    }
    static class Program
    {
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
