﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Product
{
    public partial class Form1 : Form
    {
        public List<Product> Products;
        public Form1()
        {
            InitializeComponent();
            Products = new List<Product>();
        }
        private void Products1()
        {
            Products.Add(new Product(1, "Moloco"));
            Products.Add(new Product(2, "Hleb"));
            Products.Add(new Product(3, "Shokolad"));
            Products.Add(new Product(4, "Kalbasa"));
            Products.Add(new Product(5, "Kefir"));
            Products.Add(new Product(6, "Smetana"));
            Products.Add(new Product(7, "Sosiski"));
            Products.Add(new Product(8, "Риба"));
            Products.Add(new Product(9, "Сыр"));
            Products.Add(new Product(10, "Кабачек"));
        }
        private void v()
        {
            DataTable SpisokProd = new DataTable("Product");
            DataColumn ProductID = new DataColumn("ID");
            DataColumn ProductName = new DataColumn("Name");
            SpisokProd.Columns.Add(ProductID);
            SpisokProd.Columns.Add(ProductName);
            dataGridView1.DataSource = SpisokProd;
            foreach (Product e in Products)
            {
                DataRow Product;
                Product = SpisokProd.NewRow();
                Product["ID"] = e.ProductID;
                Product["Name"] = e.ProductName;
                SpisokProd.Rows.Add(Product);
            }
        }
       
        private void V()
        { 
        
        }
      

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Products1();
            //Vuvod();
            v();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
